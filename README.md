# Spring AMQP with RabbitMQ, part3 #

## Synchronous Request/Response patterns ##

In this repo you can find demo and description of synchronous request/response patterns with Spring AMQP. 
Just briefly, we're going to look at how you can send an request into AMQP exchange and receive an reply
from another system connected into AMQP infrastructure. With Spring AMQP you can do it in the following ways:

* Receive from **temporary queue** created on request - This is uneffective and degrading performance of the application.
* Receive from **fixed queue (located in same or different exchange)** - Widely used if RabbitMQ broker doesn't support Direct-TO.
* Receive using **direct-TO** pattern - Reply is send directly into waiting channel, oh yes! For more info, see [Direct-TO](https://www.rabbitmq.com/direct-reply-to.html) 

Although direct-to is highly preffered option, we're going to make a demo of using fixed-queue request/response,
because if your AMQP broker doesn't support Direct-TO, it's your only reasonable option.

### Request/response using fixed queue ##

First, demo assumes that you created following AMQP infrastructure at RabbitMQ

**Artifacts for sending requests**

* **Exchange:** test_exchange
* **Routing Key:** greeting
* **Request queue:** greeting, binded to test_exchange

**Artifacts for sending replies**

* **Exchange:** reply_exchange
* **Routing Key:** replies
* **Reply queue:** replies, binded to reply_exchange

** Basic fixed queue and RabbitTemplate configuration idea **

To be able to receive reply from fixed queue, you need to **configure RabbitTemplate as MessageListener**. 

```
    @Bean
	public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory, Queue replies) {
		RabbitTemplate template = new RabbitTemplate(connectionFactory);
		template.setExchange("test_exchange");
		template.setRoutingKey("greeting");		
		template.setReplyAddress("reply_exchange"+"/"+replies.getName());
		template.setReplyTimeout(60000);
		return template;
	}
	
	@Bean
	public SimpleMessageListenerContainer replyContainer(ConnectionFactory connectionFactory, 
			RabbitTemplate rabbitTemplate, Queue replies) {
		SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
		container.setConnectionFactory(connectionFactory);
		container.setMessageListener(rabbitTemplate);
		container.setQueues(replies);
		return container;
	}
```
By **calling setReplyAddress** on RabbitTemplate, *spring amqp inserts the **reply address into replyTo** message property* when 
message is send. If receiving system will reply with value this property will be used to route the reply. Reply address can be in the 
form **exchange / routing** or by default replies are send into the default exchange to the queue set by setReplyQueue method. Last important configuration is the replyContainer bean, RabbitTemplate implements MessageListener interface, so *to start to receive replies from fixed 
queue, we need to **set RabbitTemplate as MessageListener on SimpleMessageListenerContainer** *.

** Sender configuration **

It's kind of straightforward:


```
/**
 * @author Tomas Kloucek
 */
public class RabbitMqSender {

	@Autowired
	private RabbitTemplate template;
	
	public void send() {
		Message message = MessageBuilder.withBody("TomasKloucek".getBytes())
				.setContentType("text/plain")
				.build();

		// greeting
		Message reply = this.template.sendAndReceive(message);
		System.out.println("Reply from server is: "+new String(reply.getBody()));
	}
}

```
Just a few notes, **always set content type at Messages** sended into AMQP, otherwise MessageListenerAdapters won't be able
to detect what's inside of payload and you won't get your payload at input of your MessageListener method. And **always use
here one of the RabbitTemplate.send-receive methods** to make the request/respsonse work, otherwise AMQP won't set be setting 
replyTO property and broker won't know where to send the reply.

And when using request/response pattern **always send messages when all beans are wired up and ready**, I had a problem with making 
this work, because I was sending when container for replies wasn't ready. See: [RabbitTemplate is not configured as Listener (stackoverflow)](http://stackoverflow.com/questions/37156811/rabbitmq-request-response-rabbittemplate-is-not-configured-as-listener)

And that's it!

### Let's test the demo ##

* git clone <this repo>
* mvn clean install
* java -jar target/demo-0.0.1-SNAPSHOT.jar

You should see following output then:

```
2016-05-14 10:22:58.770  INFO 6124 --- [cTaskExecutor-1] o.s.a.r.c.CachingConnec
tionFactory       : Created new connection: SimpleConnection@186dfeb1 [delegate=
amqp://guest@172.20.122.9:5672/]
2016-05-14 10:22:59.508  INFO 6124 --- [           main] s.b.c.e.t.TomcatEmbedde
dServletContainer : Tomcat started on port(s): 8080 (http)
2016-05-14 10:22:59.508  INFO 6124 --- [           main] demo_rabbitmq.rabbit_de
mo.App            : Started App in 4.122 seconds (JVM running for 4.587)
Reply from server is: Hello TomasKloucek
```

Hope you found this usefull.

regards

Tomas