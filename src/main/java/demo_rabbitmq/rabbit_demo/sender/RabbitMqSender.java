package demo_rabbitmq.rabbit_demo.sender;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Tomas Kloucek
 */
public class RabbitMqSender {

	@Autowired
	private RabbitTemplate template;
	
	public void send() {
		Message message = MessageBuilder.withBody("TomasKloucek".getBytes())
				.setContentType("text/plain")
				.build();

		// greeting
		Message reply = this.template.sendAndReceive(message);
		System.out.println("Reply from server is: "+new String(reply.getBody()));
	}
}

